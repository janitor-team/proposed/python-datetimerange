Installation
============

::

    pip install DateTimeRange


Dependencies
============
- Python 3.6+
- `Python package dependencies (automatically installed) <https://github.com/thombashi/DateTimeRange/network/dependencies>`__
